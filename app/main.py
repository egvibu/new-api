import json
from fastapi import FastAPI
from typing import Union

app = FastAPI()

# app.get -> GET, app.post -> POST


@app.get("/string")
def string():
    return "Hello, World!"


@app.get("/json")
def js():
    dic = {"a": 1,
           "b": 2}
    return json.dumps(dic)


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    return {"item_id": item_id, "q": q}

# @app.get("/<string>")
# def evaluate(string):
#     return eval(string + "()")
#
#
# def method2():
#     return "{ 'string': 'Hello, World!' }"


