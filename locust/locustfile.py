from locust import HttpUser, between, task
from json import JSONDecodeError


class WebsiteUser(HttpUser):
    wait_time = between(1, 5)

    @task
    def get_string(self):
        with self.client.get("/string", catch_response=True) as response:
            try:
                if response.text != "Hello, World!":
                    response.failure(f"returned wrong string: {response.text}")
            except Exception as e:
                response.failure(e.__class__.__name__)

    @task
    def get_json(self):
        with self.client.get("/json", catch_response=True) as response:
            try:
                if response.json()["a"] != 1:
                    response.failure("returned wrong value")
            except JSONDecodeError:
                response.failure("Response could not be decoded as JSON")
            except KeyError:
                response.failure("Response did not contain expected key 'a'")